﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PinballManager : Singleton<PinballManager>{

    [SerializeField]
    private Text sequence;
    public string Sequence {
    get { return sequence.text; }
    }

    private int counter;
    public const int MAXIMUM_SEQ_SIZE = 6;
    public const int SIZE_OF_SUBSEQS = 4;
	// Use this for initialization
	void Start () {
        counter = MAXIMUM_SEQ_SIZE;
	}
	
	// Update is called once per frame
	void Update () {

        for (int i = 0; i < 10; ++i) {
            if (counter >= MAXIMUM_SEQ_SIZE)
                return;

            if (Input.GetKeyDown( "["+i+"]")) {
                sequence.text += "" + i;
                counter++;
            }
        }
    }

    public bool IsSequenceComplete() {
        return counter >= MAXIMUM_SEQ_SIZE;
    }

    public void StartNewSequence() {
        int games = ConfigLoader.Instance.GetInt(ConfigLoader.GAMES_COUNT);
        ConfigLoader.Instance.SaveParameters(ConfigLoader.GAMES_COUNT, (games+1).ToString());
        sequence.text = "";
        counter = 0;
        ShowContent(true);
    }

    public List<string> GetSequences() {
        List<string> subSeqs = new List<string>();
        for (int i = 0; i <= MAXIMUM_SEQ_SIZE - SIZE_OF_SUBSEQS; i++) {
            string sub = "";
            for (int k = 0; k < SIZE_OF_SUBSEQS - 1 - i; k++) {
                sub = sequence.text.Substring(i, SIZE_OF_SUBSEQS - 1);
                sub += sequence.text.Substring(i + SIZE_OF_SUBSEQS - 1 + k, 1);
                subSeqs.Add(sub);
            }
        }
        return subSeqs;
    }

    public void ShowContent(bool visible) {
        sequence.gameObject.SetActive(visible);
    }
}
