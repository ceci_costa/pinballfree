﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class LoginManager : Singleton<LoginManager> {

    [SerializeField]
    private GameObject container;

    [SerializeField]
    private Text label;
    [SerializeField]
    private Text warning;
    [SerializeField]
    private InputField password;

    private string registeredPassword;
    
    [Header("PasswordKey")]
    public string REGISTERED_PASSWORD_KEY = "registeredPassword";

    [Header("Warning Messages")]
    public string ENTER_PASSWORD = "Digite a senha do Administrador";
    public string ENTER_NEW_PASSWORD = "Digite uma nova senha";
    public string CONFIRM_NEW_PASSWORD = "Confirme a sua senha";
    public string CONFIRMATION_FAILED = "Confirmação de senha incorreta";
    public string PASSWORD_FAILED = "Senha incorreta";

    private enum AcessState {
        INIT,
        LOGIN,
        SIGNIN,
        CONFIRM,
        LOGGED,
    }
    private AcessState state = AcessState.INIT;

    public void InitiateAccess() {

        label.text = "";
        warning.text = "";
        SetPasswordFocus();

        registeredPassword = PlayerPrefs.GetString(REGISTERED_PASSWORD_KEY, "-1");
        if (registeredPassword == "-1") {
            label.text = ENTER_NEW_PASSWORD;
            state = AcessState.SIGNIN;
        } else {
            label.text = ENTER_PASSWORD;
            state = AcessState.LOGIN;
        }
    }

    void SetPasswordFocus() {
        password.Select();
        password.ActivateInputField();
    }

    public void ShowContent(bool show = true) {
        container.SetActive(show);
    }

    public void ManageAccess(string password) {
        switch (state) {
            case AcessState.LOGIN:
                registeredPassword = PlayerPrefs.GetString(REGISTERED_PASSWORD_KEY, "-1");
                if (password == registeredPassword) {
                    state = AcessState.LOGGED;
                    ShowContent(false);
                    AdminScreen.Instance.StartSession();
                } else {
                    warning.text = PASSWORD_FAILED;
                }
                break;
            case AcessState.SIGNIN:
                registeredPassword = password;

                label.text = CONFIRM_NEW_PASSWORD;
                SetPasswordFocus();
                state = AcessState.CONFIRM;
                break;
            case AcessState.CONFIRM:
                if (registeredPassword != password) {
                    warning.text = CONFIRMATION_FAILED;

                    label.text = ENTER_NEW_PASSWORD;
                    SetPasswordFocus();
                    state = AcessState.SIGNIN;
                } else {
                    PlayerPrefs.SetString(REGISTERED_PASSWORD_KEY, password);
                    warning.text = "";

                    label.text = ENTER_PASSWORD;
                    SetPasswordFocus();
                    state = AcessState.LOGIN;
                }
                break;
            default:
                break;
        }
    }
}
