﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LoseScreen : Singleton<LoseScreen> {

    [SerializeField]
    private GameObject container;

	// Use this for initialization
	void Start () {
	    	
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    private IEnumerator CloseAfter(float time) {
        float init = Time.realtimeSinceStartup;
        while(Time.realtimeSinceStartup - init < time) {
            yield return new WaitForSeconds(0.1f);
        }
        container.SetActive(false);
        yield return null;
    }

    public void ShowContent(bool show = true) {
        container.SetActive(show);
        if(show) {
            StartCoroutine(CloseAfter(4));
        }
    }

    public bool IsActive() {
        return container.activeSelf;
    }
}
