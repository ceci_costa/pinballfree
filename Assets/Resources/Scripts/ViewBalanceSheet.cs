﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ViewBalanceSheet : MonoBehaviour {
    public GameObject container;
    public AwardInfo reference;
    public RectTransform scrollViewContent;

    public Text gamesCount;
    public Text creditedValue;
    public Text awardsCount;

    private List<AwardInfo> awardInfos = new List<AwardInfo>();

    public void ShowContent(bool show = true) {

        container.SetActive(show);

        if (show) {
            gamesCount.text = ConfigLoader.Instance.GetString(ConfigLoader.GAMES_COUNT);
            creditedValue.text = ConfigLoader.Instance.GetString(ConfigLoader.CREDITED_VALUE);
            awardsCount.text = ConfigLoader.Instance.GetString(ConfigLoader.AWARDS_COUNT);

            LoadAwardsInfo();
        }
    }

    void LoadAwardsInfo() {
        string[] codes = ConfigLoader.Instance.GetString(ConfigLoader.AWARDS_CODES).Split(' ');
        string[] dates = ConfigLoader.Instance.GetString(ConfigLoader.AWARDED_GAMES_DATES).Split(' ');
        string[] times = ConfigLoader.Instance.GetString(ConfigLoader.AWARDED_GAMES_TIMES).Split(' ');

        float infoSize = reference.GetComponent<RectTransform>().rect.height;

        foreach(AwardInfo ai in awardInfos) {
            Destroy(ai.gameObject);
            scrollViewContent.SetSizeWithCurrentAnchors(RectTransform.Axis.Vertical, 0);
        }
        awardInfos.Clear();

        for (int i = 0; i < codes.Length && i < dates.Length && i < times.Length; i++) {
            if (codes[i] == "" || dates[i] == "" || times[i] == "")
                continue;

            AwardInfo info = Instantiate(reference);

            info.transform.SetParent(scrollViewContent.transform);
            info.transform.localScale = new Vector3(1, 1, 1);
            info.transform.position = new Vector3(0, 0, 0);

            info.index.text = (i + 1).ToString();
            info.match.text = codes[i];
            info.date.text = dates[i];
            info.time.text = times[i];

            awardInfos.Add(info);
            scrollViewContent.SetSizeWithCurrentAnchors(RectTransform.Axis.Vertical, (i+1) * infoSize);
        }
    }

    public void CleanData() {
        ConfigLoader.Instance.SaveParameters(ConfigLoader.GAMES_COUNT, "0");
        ConfigLoader.Instance.SaveParameters(ConfigLoader.CREDITED_VALUE, "0");
        ConfigLoader.Instance.SaveParameters(ConfigLoader.AWARDS_COUNT, "0");

        ConfigLoader.Instance.SaveParameters(ConfigLoader.AWARDS_CODES, "");
        ConfigLoader.Instance.SaveParameters(ConfigLoader.AWARDED_GAMES_DATES, "");
        ConfigLoader.Instance.SaveParameters(ConfigLoader.AWARDED_GAMES_TIMES, "");
        ShowContent(true);
    }
}
