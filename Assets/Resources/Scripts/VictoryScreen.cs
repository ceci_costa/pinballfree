﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class VictoryScreen : Singleton<VictoryScreen> {

    [SerializeField]
    private GameObject container;
    [SerializeField]
    private Text sequence;
    public string Sequence {
        set { sequence.text = value; }
    }

    [SerializeField]
    private Text matches;
    [SerializeField]
    private Text subSequences;

    private List<string> awards = new List<string>();
    public List<string> Awards {
        get { return awards; }
        set { awards = value; }
    }
    private List<string> subSeqs = new List<string>();
    public List<string> SubSeqs {
        get { return subSeqs; }
        set { subSeqs = value; }
    }

   
    public void ShowContent(bool visible) {
        container.SetActive(visible);
        
        AwardList.Instance.UpdateAwards();
        AwardList.Instance.SetInsteractable(false);
        AwardList.Instance.SetVisible(visible);

        matches.text = "";
        foreach (string s in awards) {
            matches.text += s + " ";
        }

        subSequences.text = "";
        foreach (string s in subSeqs) {
            subSequences.text += s + " ";
        }
    }

}
