﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;


public class AppGeneralManager : Singleton<AppGeneralManager> {

    public Text title;
    public AudioSource presentation;
    public enum PBFState {
        PRESENTATION,
        CREDIT_SCREEN,
        GAME_STARTED,
        PLAYING,
        GAME_ENDING,
        VICTORY,
        PAYMENT,
        LOSE,
        ADMIN_ACCESS,
    }
    private PBFState state = PBFState.PRESENTATION;
    private float waitToReplay = 120;
    private float lastPlay = - 120;

    // Use this for initialization
    void Start() {
        Screen.sleepTimeout = SleepTimeout.NeverSleep;
        state = PBFState.PRESENTATION;
    }

    // Update is called once per frame
    void Update() {
        //EnterAdminPaymentInput();
        //EnterAdminOptionsInput();
        InsertCreditInput();

        switch (state) {
            case PBFState.PRESENTATION:
                title.text = "Insira Créditos";
                PlayPresentation();
                if (CreditManager.Instance.GetCredits() > 0) {
                    CreditManager.Instance.ConsumeCredit();
                    state = PBFState.GAME_STARTED;
                }
                break;
            case PBFState.GAME_STARTED:
                title.text = "Partida Iniciada";
                PinballManager.Instance.StartNewSequence();
                state = PBFState.PLAYING;
                break;
            case PBFState.PLAYING:
                state = GameplayRoutine();
                break;
            case PBFState.VICTORY:
                title.text = "Parabéns! Você acertou a milhar! Procure um atendente para receber seu prêmio.";
                if (!AdminScreen.Instance.IsLoggedOut()) {
                    state = PBFState.PAYMENT;
                }
                break;
            case PBFState.PAYMENT:
                title.text = "Pagamento do Prêmio";
                if (AdminScreen.Instance.IsLoggedOut()) {
                    state = PBFState.PRESENTATION;
                }
                break;
            case PBFState.ADMIN_ACCESS:
                title.text = "Administrador";
                if (AdminScreen.Instance.IsLoggedOut()) {
                    state = PBFState.PRESENTATION;
                }
                break;
            case PBFState.LOSE:
                title.text = "Sem premiações";
                if (!LoseScreen.Instance.IsActive()) {
                    state = PBFState.PRESENTATION;
                }
                break;
            default:
                break;   
         }
    }

    void PlayPresentation() {
        if(Time.time - lastPlay > waitToReplay) {
            lastPlay = Time.time;
            presentation.Play();
        }
    }

    PBFState GameplayRoutine() {
        if (!PinballManager.Instance.IsSequenceComplete()) {
            return PBFState.PLAYING;
        }

        List<string> seqs = PinballManager.Instance.GetSequences();
        List<string> won = new List<string>();
        foreach (string s in seqs) {
            if (AwardList.Instance.GetAward(s) != null) {
                won.Add(s);
            }
        }
        if (won.Count > 0) {
            PinballManager.Instance.ShowContent(false);
            VictoryScreen.Instance.SubSeqs = seqs;
            VictoryScreen.Instance.Awards = won;
            VictoryScreen.Instance.Sequence = PinballManager.Instance.Sequence;
            VictoryScreen.Instance.ShowContent(true);
            return PBFState.VICTORY;
        }
        PinballManager.Instance.ShowContent(false);
        LoseScreen.Instance.ShowContent(true);
        return PBFState.LOSE;
    }

    public void EnterAdminPaymentInput() {
        if (state != PBFState.VICTORY)
            return;

        //if (Input.GetMouseButtonDown(0)) {
            VictoryScreen.Instance.ShowContent(false);
            AdminScreen.Instance.OpenGridPaymentAccess();
            state = PBFState.PAYMENT;
        //}

    }

    public void EnterAdminOptionsInput() {
        if (state != PBFState.PRESENTATION)
            return;

        //if (Input.GetMouseButtonDown(0)) {
            AdminScreen.Instance.OpenGridAdminAccess();
            state = PBFState.ADMIN_ACCESS;
        //}

    }
   
    void InsertCreditInput() {
        if (TouchScreenKeyboard.visible)
            return;
        if (Input.GetKeyDown(KeyCode.C)) {
            CreditManager.Instance.InsertCash();
        }
    }

}

