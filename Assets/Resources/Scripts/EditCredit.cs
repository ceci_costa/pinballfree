﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class EditCredit : MonoBehaviour {

    public InputField editCredit;
	// Use this for initialization
	void Start () {
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    void OnEnable() {
        editCredit.text = ConfigLoader.Instance.GetString(ConfigLoader.CREDIT_COST);
    }

    public void RegisterCreditValue(string value) {
        if (int.Parse(value) <= 0)
            return;
        ConfigLoader.Instance.SaveParameters(ConfigLoader.CREDIT_COST, value);
    }
}
