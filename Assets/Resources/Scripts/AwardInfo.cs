﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class AwardInfo : MonoBehaviour {

    public Text index;
    public Text match;
    public Text date;
    public Text time;
}
