﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PaymentScreen : Singleton<PaymentScreen> {

    [SerializeField]
    private GameObject container;
    [SerializeField]
    private Text matches;
    public string Matches {
        get { return matches.text;  }
        set { matches.text = value; }
    }
    [SerializeField]
    private Text data;
    [SerializeField]
    private Text time;

    public void UpdatePaymentInfo() {
        System.DateTime dateTime = System.DateTime.Now;
        data.text = dateTime.Day + "/" + dateTime.Month + "/" + dateTime.Year;
        time.text = dateTime.Hour + ":" + dateTime.Minute + ":" + dateTime.Second;

        matches.text = "";
        foreach (string s in VictoryScreen.Instance.Awards) {
            matches.text += s + " ";
            SaveStatistics(s, data.text, time.text);
        }
    }

    public void ShowContent(bool show = true) {
        container.SetActive(show);

        if(show) {
            UpdatePaymentInfo();
        }
    }

    void SaveStatistics(string match, string date, string time) {
        string codes = ConfigLoader.Instance.GetString(ConfigLoader.AWARDS_CODES);
        string dates = ConfigLoader.Instance.GetString(ConfigLoader.AWARDED_GAMES_DATES);
        string times = ConfigLoader.Instance.GetString(ConfigLoader.AWARDED_GAMES_TIMES);

        codes += match + " ";
        dates += date + " ";
        times += time + " ";

        ConfigLoader.Instance.SaveParameters(ConfigLoader.AWARDS_CODES, codes);
        ConfigLoader.Instance.SaveParameters(ConfigLoader.AWARDED_GAMES_DATES, dates);
        ConfigLoader.Instance.SaveParameters(ConfigLoader.AWARDED_GAMES_TIMES, times);


        int awardsCount = ConfigLoader.Instance.GetInt(ConfigLoader.AWARDS_COUNT);
        ConfigLoader.Instance.SaveParameters(ConfigLoader.AWARDS_COUNT, (awardsCount + 1).ToString());
    }
}
