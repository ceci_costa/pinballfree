﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;
using UnityEngine.Events;

// Class declaration
[System.Serializable]
public class CellEvent : UnityEvent<string> { }

public class CellInput : MonoBehaviour, IPointerEnterHandler, IPointerUpHandler, IPointerDownHandler {

    public int value;
    public int minimumSize;
    public Text warning;
    
    public CellEvent sequenceStarted;
    public CellEvent sequenceFinished;

    internal enum InputState {
        WAITING,
        PRESSED,
    }
    internal static InputState gridInputState = InputState.WAITING;
    internal static string sequence;
    private Image image;
	// Use this for initialization
	void Start () {
        image = GetComponent<Image>();
        Physics2D.queriesHitTriggers = true;
	}
	
    void OnEnable() {
        gridInputState = InputState.WAITING;
        warning.text = "";
        sequence = "";
    }

	// Update is called once per frame
	void Update () {
        if (gridInputState == InputState.WAITING) 
            image.color = Color.gray;
	}

    public void OnPointerDown(PointerEventData eventData) {
        gridInputState = InputState.PRESSED;
        image.color = Color.blue;
        sequence += value;
        sequenceStarted.Invoke(sequence);
    }

    public void OnPointerEnter(PointerEventData eventData) {
        if (gridInputState != InputState.PRESSED)
            return;
        image.color = Color.green;
        sequence += value;
    }

    public void OnPointerUp(PointerEventData eventData) {
        gridInputState = InputState.WAITING;
        image.color = Color.yellow;

        if(sequence.Length < minimumSize) {
            warning.text = "O desenho deve ter pelo menos" + minimumSize + " pontos.";
            return;
        }
        sequenceFinished.Invoke(sequence);
        sequence = "";
    }

    
}
