﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class CreditManager : Singleton<CreditManager>{

    public Text creditInsert;
    public Text cashInsert;
    public Text creditCost;
    public AudioSource audioSignal;

    private int cash = 0;

    void Start() {
        if(GetCreditsCost() <= 0)
            ConfigLoader.Instance.SaveParameters(ConfigLoader.CREDIT_COST, "1");
    }

    void Update () {
        creditInsert.text = GetCredits().ToString();
        cashInsert.text = cash.ToString() + ",00";
        creditCost.text = GetCreditsCost().ToString() + ",00";

        //TODO: Optimize to not read the credit value from player prefs every frame
	}

    public int GetCredits() {
        return cash / GetCreditsCost();
    }

    public int GetCreditsCost() {
        int note = ConfigLoader.Instance.GetInt(ConfigLoader.CREDIT_COST);
        return note;
    }

    public void InsertCash() {
        cash++;
        int creditedValue = ConfigLoader.Instance.GetInt(ConfigLoader.CREDITED_VALUE);
        ConfigLoader.Instance.SaveParameters(ConfigLoader.CREDITED_VALUE, (creditedValue + 1).ToString());
    }

    public void ConsumeCredit() {
        if (cash - GetCreditsCost() < 0)
            return;
        cash -= GetCreditsCost();
        audioSignal.Play();
    }
}
