﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AwardList : Singleton<AwardList> {

    public List<AwardItem> awards;
    [SerializeField]
    private GameObject container;

    void Start() {
        UpdateAwards();
    }

    public void UpdateAwards() {
        Pair<string, string>[] awards = ConfigLoader.Instance.awards;
        int i = 0;
        foreach(Pair<string, string> s in awards) {
            if (i > this.awards.Count)
                break;
            if(s == null) {
                continue;
            }
            this.awards[i].number.text = s.First;
            this.awards[i].description.text = s.Second;
            i++;
        }
    }

    public void SaveAwards() {
        int k = 0;
        foreach(AwardItem i in awards) {
            ConfigLoader.Instance.SetAwardAt(k++, i.number.text, i.description.text);
        }
        ConfigLoader.Instance.SaveAwards();
        UpdateAwards();
        
    }

    public string GetAward(string key) {
        foreach (AwardItem i in awards) {
            if (i == null)
                continue;
            if (i.number.text == key)
                return i.description.text;
        }
        return null;
    }

    public void SetInsteractable(bool interactable) {
        foreach(AwardItem i in awards) {
            if (i == null)
                continue;
            i.number.interactable = interactable;
            i.description.interactable = interactable;
        }
    }

    public void SetVisible(bool visible) {
        UpdateAwards();
        container.SetActive(visible);
    }
}
