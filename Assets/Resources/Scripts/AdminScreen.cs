﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class AdminScreen : Singleton<AdminScreen> {

    public GameObject options;
    public LoginManager adminLoginManager;
    public LoginManager manLoginManager;
    public LoginManager adminGridLoginManager;
    public LoginManager manGridLoginManager;

    private bool isActive = false;
    private int mode = 0;

    public void OpenAdminOptions() {
        adminLoginManager.ShowContent(true);
        adminLoginManager.InitiateAccess();
        isActive = true;
        mode = 0;
    }

    public void OpenPayment() {
        manLoginManager.ShowContent(true);
        manLoginManager.InitiateAccess();
        isActive = true;
        mode = 1;
    }

    public void OpenGridPaymentAccess() {
        manGridLoginManager.ShowContent(true);
        manGridLoginManager.InitiateAccess();
        isActive = true;
        mode = 2;
    }

    public void OpenGridAdminAccess() {
        adminGridLoginManager.ShowContent(true);
        adminGridLoginManager.InitiateAccess();
        isActive = true;
        mode = 3;
    }

    public void CloseAdmin() {
        options.SetActive(false);
        PaymentScreen.Instance.ShowContent(false);
        adminLoginManager.ShowContent(false);
        manLoginManager.ShowContent(false);
        adminGridLoginManager.ShowContent(false);
        manGridLoginManager.ShowContent(false);
        isActive = false;
    }

    public bool IsLoggedOut() {
        return !isActive;
    }

    public void AccessAdmin(string password) {
        adminLoginManager.ManageAccess(password);
    }

    internal void StartSession() {
        ShowPayment(false);
        adminLoginManager.ShowContent(false);
        manLoginManager.ShowContent(false);
        adminGridLoginManager.ShowContent(false);
        manGridLoginManager.ShowContent(false);

        switch (mode) {
            case 0:
                ShowOptions();
                break;
            case 1:
                ShowPayment(true);
                break;
            case 2:
                OpenPayment();
                break;
            case 3:
                OpenAdminOptions();
                break;
            default:
                break;
        }
    }

    private void ShowOptions(bool show = true) {
        options.SetActive(show);
    }

    private void ShowPayment(bool show = true) {
        PaymentScreen.Instance.ShowContent(show);
    }

    public void OpenAwardsPanel() {
        AwardList.Instance.SetInsteractable(true);
        AwardList.Instance.SetVisible(true);
    }

    public void CloseAwardsPanel() {
        AwardList.Instance.SetInsteractable(false);
        AwardList.Instance.SetVisible(false);
    }

    public void SaveAwards() {
        AwardList.Instance.SaveAwards();
    }
}
