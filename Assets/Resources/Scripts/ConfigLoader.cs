﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Pair<T, U> {
    public Pair() {
    }

    public Pair(T first, U second) {
        this.First = first;
        this.Second = second;
    }

    public T First { get; set; }
    public U Second { get; set; }
};

public class ConfigLoader : Singleton<ConfigLoader> {

    public const int MAX_AWARDS = 20;

    public const string CREDIT_COST = "credit_cost";
    public const string GAMES_COUNT = "games_count";
    public const string CREDITED_VALUE = "credited_value";
    public const string AWARDS_COUNT = "awards_count";
    public const string AWARDS_CODES = "awards_codes";
    public const string AWARDED_GAMES_TIMES = "awarded_games_times";
    public const string AWARDED_GAMES_DATES = "awarded_games_dates";

    private const string AWARDS_FILE = "pinballAwards";
    
    public Pair<string, string>[] awards = new Pair<string, string>[MAX_AWARDS];
    public Dictionary<string, string> parameters = new Dictionary<string, string>();

    // Use this for initialization
    void Awake () {
        LoadAwards();
    }

    /// <summary>
    /// Save an award to the list of award. This change will not persist until SaveAwards is called
    /// </summary>
    /// <param name="index"> index of the list to save or the award. It will subscribe the previous value and key of the award</param>
    /// <param name="key"> the sequence value for this award</param>
    /// <param name="value">the description of the award</param>
    public void SetAwardAt(int index, string key, string value) {
        if (index >= MAX_AWARDS)
            return;
        Pair<string, string> pair = new Pair<string, string>(key, value);
        awards[index] = pair;
        
    }

    /// <summary>
    /// Save a configuration parameter
    /// </summary>
    /// <param name="parameter"></param>
    /// <param name="value"></param>
    public void SaveParameters(string parameter, string value) {
        PlayerPrefs.SetString(parameter, value);
        parameters.Remove(parameter);
        parameters.Add(parameter, value);
    }

    /// <summary>
    /// Load all award from the app associated data
    /// </summary>
    void LoadAwards() {
        string configs = PlayerPrefs.GetString(AWARDS_FILE);
        string[] args = System.Text.RegularExpressions.Regex.Split(configs, "\r\n|\r|\n");

        int i = 0;
        foreach (string s in args) {
            string[] arg = s.Split('=');
            if (arg.Length < 2) {
                continue;
            }
            SetAwardAt(i++, arg[0], arg[1]);
        }
    }

    /// <summary>
    /// Save the awards list to the app associated data
    /// </summary>
    public void SaveAwards() {
        string toWrite = "";
        foreach(Pair<string, string> p in awards) {
            toWrite += p.First + "=" + p.Second + "\n";
        }
        PlayerPrefs.SetString(AWARDS_FILE, toWrite);
    }

    /// <summary>
    /// Access method for parameters
    /// </summary>
    public int GetInt(string key) {
        return int.Parse(GetString(key) == "" ? "0" : GetString(key));
    }
    /// <summary>
    /// Access method for parameters
    /// </summary>
    public float GetFloat(string key) {
        return float.Parse(GetString(key) == ""? "0": GetString(key));
    }
    /// <summary>
    /// Access method for parameters
    /// </summary>
    public string GetString(string key) {
        if (parameters.ContainsKey(key))
            return parameters[key];

        if (PlayerPrefs.GetString(key, "") != "")
            parameters.Add(key, PlayerPrefs.GetString(key));
        else
            return "";
        return parameters[key];
    }
}
